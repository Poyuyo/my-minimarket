package com.folcademy.myminimarket.services;

import com.folcademy.myminimarket.exceptions.NotFoundException;
import com.folcademy.myminimarket.models.dto.*;
import com.folcademy.myminimarket.models.entities.BrandEntity;
import com.folcademy.myminimarket.models.entities.CategoryEntity;
import com.folcademy.myminimarket.models.entities.ProductEntity;
import com.folcademy.myminimarket.models.mappers.BrandMapper;
import com.folcademy.myminimarket.models.mappers.CategoryMapper;
import com.folcademy.myminimarket.models.mappers.ProductMapper;
import com.folcademy.myminimarket.models.repositories.BrandRepository;
import com.folcademy.myminimarket.models.repositories.CategoryRepository;
import com.folcademy.myminimarket.models.repositories.ProductRepository;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
public class ProductService {

    private final ProductRepository productRepository;
    private final ProductMapper productMapper;
    private final CategoryRepository categoryRepository;
    private final BrandRepository brandRepository;
    private final CategoryMapper categoryMapper;
    private final BrandMapper brandMapper;

    public ProductService(ProductRepository productRepository,
                          ProductMapper productMapper,
                          CategoryRepository categoryRepository,
                          BrandRepository brandRepository,
                          CategoryMapper categoryMapper,
                          BrandMapper brandMapper) {
        this.productRepository = productRepository;
        this.productMapper = productMapper;
        this.categoryRepository = categoryRepository;
        this.brandRepository = brandRepository;
        this.categoryMapper = categoryMapper;
        this.brandMapper = brandMapper;
    }


    public List<ProductDTO> getProducts() {
        List<ProductEntity> productEntities = productRepository.findAll();
        List<ProductDTO> productDTOS = new ArrayList<>();
        for (ProductEntity productEntity : productEntities) {
            if (productEntity.getDeleted() == null) {
                CategoryDTO categoryDTO = categoryMapper.mapCategoryEntityToCategoryDto(productEntity.getCategory());
                BrandDTO brandDTO = brandMapper.mapBrandEntityToBrandDTO(productEntity.getBrand());
                productDTOS.add(productMapper.mapProductEntityToProductDto(productEntity, categoryDTO, brandDTO));
            }
        }

        return productDTOS;
    }

    public ProductDetailDTO getProduct(int id) {
        Optional<ProductEntity> optionalProductEntity = productRepository.findById(id);

        if (optionalProductEntity.isPresent()) {
            ProductEntity productEntity = optionalProductEntity.get();
            ProductDetailDTO productDetailDTO = productMapper.mapProductEntityToProductDetailDto(productEntity);
            return productDetailDTO;
        } else {
            throw new NotFoundException("No existe producto");
        }
    }


    public ResponseEntity<String> createProduct(ProductNewDTO productNewDTO) {

        ProductEntity productEntity = productMapper.mapProductNewDtoToProductEntity(productNewDTO,
                findCategory(productNewDTO.getCategory_id()),
                findBrand(productNewDTO.getBrand_id()));
        productRepository.save(productEntity);

        return new ResponseEntity<>("Created", HttpStatus.OK);
    }


    public ResponseEntity<String> editProduct(ProductEditDTO productEditDTO) {
        Optional<ProductEntity> optionalProductEntity = productRepository.findById(productEditDTO.getId());

        if (optionalProductEntity.isPresent()) {

            ProductEntity productEntity = productMapper.mapProductEditDtoToProductEntity(productEditDTO,
                    findCategory(productEditDTO.getCategory_id()),
                    findBrand(productEditDTO.getBrand_id()),
                    optionalProductEntity.get().getDeleted(),
                    optionalProductEntity.get().getCreated());

            productEntity.setId(productEditDTO.getId());

            productRepository.save(productEntity);
            return new ResponseEntity<>("Modified", HttpStatus.OK);
        } else {
            throw new NotFoundException("No existe producto");
        }
    }


    public ResponseEntity<String> deleteProduct(int id) {
        Optional<ProductEntity> optionalProductEntity = productRepository.findById(id);

        if (optionalProductEntity.isPresent()) {
            ProductEntity productEntity = optionalProductEntity.get();

            productEntity.setDeleted(new Date());
            productRepository.save(productEntity);
            return new ResponseEntity<>("Deleted", HttpStatus.OK);
        } else {
            throw new NotFoundException("No existe producto");
        }
    }

    public CategoryEntity findCategory(int category_id) {
        Optional<CategoryEntity> optionalCategoryEntity = categoryRepository.findById(category_id);

        if (optionalCategoryEntity.isEmpty()) {
            throw new NotFoundException("No existe category");
        }
        return optionalCategoryEntity.get();
    }

    public BrandEntity findBrand(int brand_id) {
        Optional<BrandEntity> optionalBrandEntity = brandRepository.findById(brand_id);

        if (optionalBrandEntity.isEmpty()) {
            throw new NotFoundException("No existe brand");
        }
        return optionalBrandEntity.get();
    }

}
