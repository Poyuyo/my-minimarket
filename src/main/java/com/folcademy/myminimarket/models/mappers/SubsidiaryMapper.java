package com.folcademy.myminimarket.models.mappers;

import com.folcademy.myminimarket.models.dto.SubsidiaryDTO;
import com.folcademy.myminimarket.models.dto.SubsidiaryDetailDTO;
import com.folcademy.myminimarket.models.dto.SubsidiaryEditDTO;
import com.folcademy.myminimarket.models.dto.SubsidiaryNewDTO;
import com.folcademy.myminimarket.models.entities.SubsidiaryEntity;
import org.springframework.stereotype.Component;

import java.util.Date;

@Component
public class SubsidiaryMapper {
    public SubsidiaryDTO mapSubsidiaryEntityToSubsidiaryDTO(SubsidiaryEntity subsidiaryEntity) {
        return new SubsidiaryDTO(subsidiaryEntity.getId(),
                subsidiaryEntity.getName(),
                subsidiaryEntity.getAddress(),
                subsidiaryEntity.getCity());
    }

    public SubsidiaryDetailDTO mapSubsidiaryEntityToSubsidiaryDetailDTO(SubsidiaryEntity subsidiaryEntity) {
        return new SubsidiaryDetailDTO(subsidiaryEntity.getName(),
                subsidiaryEntity.getAddress(),
                subsidiaryEntity.getCity(),
                subsidiaryEntity.getDeleted(),
                subsidiaryEntity.getCreated(),
                subsidiaryEntity.getModified());
    }

    public SubsidiaryEntity mapSubsidiaryNewDTOToSubsidiaryEntity(SubsidiaryNewDTO subsidiaryNewDTO) {
        return new SubsidiaryEntity(subsidiaryNewDTO.getName(),
                subsidiaryNewDTO.getAddress(),
                subsidiaryNewDTO.getCity(),
                null,
                new Date(),
                new Date());
    }

    public SubsidiaryEntity mapSubsidiaryEditDTOToSubsidiaryEntity(SubsidiaryEditDTO subsidiaryEditDTO, Date deleted, Date created){
        return new SubsidiaryEntity(subsidiaryEditDTO.getName(),
                subsidiaryEditDTO.getAddress(),
                subsidiaryEditDTO.getCity(),
                deleted,
                created,
                new Date());
    }
}
