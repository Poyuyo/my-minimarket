package com.folcademy.myminimarket.models.mappers;

import com.folcademy.myminimarket.models.dto.CategoryDTO;
import com.folcademy.myminimarket.models.dto.CategoryDetailDTO;
import com.folcademy.myminimarket.models.dto.CategoryNewDTO;
import com.folcademy.myminimarket.models.entities.CategoryEntity;
import org.springframework.stereotype.Component;

import java.util.Date;

@Component
public class CategoryMapper {

    public CategoryDTO mapCategoryEntityToCategoryDto(CategoryEntity categoryEntity) {
        return new CategoryDTO(categoryEntity.getId(), categoryEntity.getName());
    }

    public CategoryDetailDTO mapCategoryEntityToCategoryDetailDto(CategoryEntity categoryEntity) {
        return new CategoryDetailDTO(categoryEntity.getName(),
                categoryEntity.getCreated(),
                categoryEntity.getModified(),
                categoryEntity.getDeleted());
    }

    public CategoryEntity mapCategoryNewDtoToCategoryEntity(CategoryNewDTO categoryNewDTO) {
        return new CategoryEntity(categoryNewDTO.getName(), new Date(), new Date(), null );
    }
}
