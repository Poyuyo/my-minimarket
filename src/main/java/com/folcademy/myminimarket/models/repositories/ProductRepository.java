package com.folcademy.myminimarket.models.repositories;

import com.folcademy.myminimarket.models.entities.ProductEntity;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;

public interface ProductRepository extends CrudRepository<ProductEntity, String> {
    List<ProductEntity> findAll();
    Optional<ProductEntity> findById(int id);
    boolean existsById(Integer id);
}
