package com.folcademy.myminimarket.models.dto;

public class SaleItemDTO {

    private int id;
    private int amount;
    private int sale_id;
    private ProductDTO product;

    public SaleItemDTO() {
    }

    public SaleItemDTO(int id, int amount, int sale_id, ProductDTO product) {
        this.id = id;
        this.amount = amount;
        this.sale_id = sale_id;
        this.product = product;
    }

    public int getId() {
        return id;
    }

    public int getAmount() {
        return amount;
    }

    public int getSale_id() {
        return sale_id;
    }

    public ProductDTO getProduct() {
        return product;
    }
}
