package com.folcademy.myminimarket.models.entities;

import org.apache.catalina.User;

import javax.persistence.*;
import java.util.Date;

@Entity(name = "stocks")
public class StockEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;
    private int amount;
    private String observations;
    @ManyToOne()
    @JoinColumn(name = "product_id")
    private ProductEntity product;
    @ManyToOne()
    @JoinColumn(name = "subsidiary_id")
    private SubsidiaryEntity subsidiary;
    @ManyToOne()
    @JoinColumn(name = "user_id")
    private UserEntity user;
    @Temporal(TemporalType.DATE)
    private Date deleted;
    @Temporal(TemporalType.DATE)
    private Date created;
    @Temporal(TemporalType.DATE)
    private Date modified;

    public StockEntity() {
    }

    public StockEntity(int amount, String observations, ProductEntity product, SubsidiaryEntity subsidiary, UserEntity user, Date deleted, Date created, Date modified) {
        this.amount = amount;
        this.observations = observations;
        this.product = product;
        this.subsidiary = subsidiary;
        this.user = user;
        this.deleted = deleted;
        this.created = created;
        this.modified = modified;
    }

    public int getId() {
        return id;
    }

    public int getAmount() {
        return amount;
    }

    public String getObservations() {
        return observations;
    }

    public ProductEntity getProduct() {
        return product;
    }

    public SubsidiaryEntity getSubsidiary() {
        return subsidiary;
    }

    public UserEntity getUser() {
        return user;
    }

    public Date getDeleted() {
        return deleted;
    }

    public Date getCreated() {
        return created;
    }

    public Date getModified() {
        return modified;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setModified(Date modified) {
        this.modified = modified;
    }

    public void setDeleted(Date deleted) {
        this.deleted = deleted;
    }
}
