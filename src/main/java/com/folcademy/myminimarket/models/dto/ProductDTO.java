package com.folcademy.myminimarket.models.dto;

import com.folcademy.myminimarket.models.entities.BrandEntity;
import com.folcademy.myminimarket.models.entities.CategoryEntity;

import java.math.BigDecimal;
import java.util.Date;

public class ProductDTO {
    private Integer id;
    private String name;
    private String observations;
    private String measure_unit;
    private BigDecimal unit_amount;
    private CategoryDTO category;
    private BrandDTO brand;
    private Date expiration_data;

    public ProductDTO() {
    }

    public ProductDTO(int id, String name, String observations, String measure_unit, BigDecimal unit_amount, CategoryDTO category, BrandDTO brand, Date expiration_data) {
        this.id = id;
        this.name = name;
        this.observations = observations;
        this.measure_unit = measure_unit;
        this.unit_amount = unit_amount;
        this.category = category;
        this.brand = brand;
        this.expiration_data = expiration_data;
    }

    public Integer getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getObservations() {
        return observations;
    }

    public String getMeasure_unit() {
        return measure_unit;
    }

    public BigDecimal getUnit_amount() {
        return unit_amount;
    }

    public CategoryDTO getCategory() {
        return category;
    }

    public BrandDTO getBrand() {
        return brand;
    }

    public Date getExpiration_data() {
        return expiration_data;
    }
}
