package com.folcademy.myminimarket.models.repositories;

import com.folcademy.myminimarket.models.entities.SaleEntity;
import com.folcademy.myminimarket.models.entities.SaleItemEntity;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;

public interface SaleRepository extends CrudRepository<SaleEntity, String> {
    List<SaleEntity> findAll();

    Optional<SaleEntity> findById(int id);
}
