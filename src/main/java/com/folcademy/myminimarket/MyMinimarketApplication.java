package com.folcademy.myminimarket;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MyMinimarketApplication {

	public static void main(String[] args) {
		SpringApplication.run(MyMinimarketApplication.class, args);
	}

}
