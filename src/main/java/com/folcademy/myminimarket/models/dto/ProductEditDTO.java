package com.folcademy.myminimarket.models.dto;

import com.folcademy.myminimarket.models.enums.MeasureUnit;

import java.math.BigDecimal;
import java.util.Date;

public class ProductEditDTO {
    private int id;
    private String name;
    private String observations;
    private MeasureUnit measure_unit;
    private BigDecimal unit_amount;
    private int category_id;
    private int brand_id;
    private Date expiration_data;

    public ProductEditDTO() {
    }

    public ProductEditDTO(int id,
                          String name,
                          String observations,
                          MeasureUnit measure_unit,
                          BigDecimal unit_amount,
                          int category_id,
                          int brand_id,
                          Date expiration_data) {
        this.id = id;
        this.name = name;
        this.observations = observations;
        this.measure_unit = measure_unit;
        this.unit_amount = unit_amount;
        this.category_id = category_id;
        this.brand_id = brand_id;
        this.expiration_data = expiration_data;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getObservations() {
        return observations;
    }

    public MeasureUnit getMeasure_unit() {
        return measure_unit;
    }

    public BigDecimal getUnit_amount() {
        return unit_amount;
    }

    public int getCategory_id() {
        return category_id;
    }

    public int getBrand_id() {
        return brand_id;
    }

    public Date getExpiration_data() {
        return expiration_data;
    }
}
