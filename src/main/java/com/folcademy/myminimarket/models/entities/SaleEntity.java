package com.folcademy.myminimarket.models.entities;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Entity(name = "sales")
public class SaleEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;
    private String state;
    @ManyToOne()
    @JoinColumn(name = "subsidiary_id")
    private SubsidiaryEntity subsidiary;
    @ManyToOne()
    @JoinColumn(name = "user_id")
    private UserEntity user;
    @OneToMany(orphanRemoval = true, cascade = CascadeType.ALL)
    @JoinColumn(name = "sale_id")
    List<SaleItemEntity> saleItems;
    @Temporal(TemporalType.DATE)
    private Date deleted;
    @Temporal(TemporalType.DATE)
    private Date created;
    @Temporal(TemporalType.DATE)
    private Date modified;

    public SaleEntity() {
    }

    public SaleEntity(String state, SubsidiaryEntity subsidiary, UserEntity user, List<SaleItemEntity> saleItems, Date deleted, Date created, Date modified) {
        this.state = state;
        this.subsidiary = subsidiary;
        this.user = user;
        this.saleItems = saleItems;
        this.deleted = deleted;
        this.created = created;
        this.modified = modified;
    }

    public int getId() {
        return id;
    }

    public String getState() {
        return state;
    }

    public SubsidiaryEntity getSubsidiary() {
        return subsidiary;
    }

    public UserEntity getUser() {
        return user;
    }

    public List<SaleItemEntity> getSaleItems() {
        return saleItems;
    }

    public Date getDeleted() {
        return deleted;
    }

    public Date getCreated() {
        return created;
    }

    public Date getModified() {
        return modified;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setDeleted(Date deleted) {
        this.deleted = deleted;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public void setModified(Date modified) {
        this.modified = modified;
    }
}
