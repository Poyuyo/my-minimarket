package com.folcademy.myminimarket.models.repositories;

import com.folcademy.myminimarket.models.entities.SaleEntity;
import org.springframework.data.repository.PagingAndSortingRepository;

import javax.swing.text.html.parser.Entity;
import java.util.Date;
import java.util.List;

public interface ReportRepository extends  PagingAndSortingRepository<SaleEntity,Integer> {
    List<SaleEntity> findAllByCreatedBetween(Date start, Date end);
}
