package com.folcademy.myminimarket.models.dto;

import java.util.Date;

public class StockDetailDTO {

    private ProductDTO product;
    private int stock;

    public StockDetailDTO() {
    }

    public StockDetailDTO(ProductDTO product, int stock) {
        this.product = product;
        this.stock = stock;
    }

    public ProductDTO getProduct() {
        return product;
    }

    public int getStock() {
        return stock;
    }
}
