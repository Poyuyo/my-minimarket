package com.folcademy.myminimarket.models.dto;

import java.math.BigDecimal;
import java.util.List;

public class SaleDTO {

    private int id;
    private String state;
    private SubsidiaryDTO subsidiary;
    private UserMinDTO user;
    private List<SaleItemDTO> saleItemDTOS;
    private BigDecimal totalAmount;

    public SaleDTO() {
    }

    public SaleDTO(int id, String state, SubsidiaryDTO subsidiary, UserMinDTO user, List<SaleItemDTO> saleItemDTOS, BigDecimal totalAmount) {
        this.id = id;
        this.state = state;
        this.subsidiary = subsidiary;
        this.user = user;
        this.saleItemDTOS = saleItemDTOS;
        this.totalAmount = totalAmount;
    }

    public int getId() {
        return id;
    }

    public String getState() {
        return state;
    }

    public SubsidiaryDTO getSubsidiary() {
        return subsidiary;
    }

    public UserMinDTO getUser() {
        return user;
    }

    public List<SaleItemDTO> getSaleItemDTOS() {
        return saleItemDTOS;
    }

    public BigDecimal getTotalAmount() {
        return totalAmount;
    }
}
