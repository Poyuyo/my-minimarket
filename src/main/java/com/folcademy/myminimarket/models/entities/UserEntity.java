package com.folcademy.myminimarket.models.entities;

import javax.persistence.*;
import java.util.Date;

@Entity(name = "users")
public class UserEntity {
    @Id
    private int dni;
    private String name;
    private String username;
    private String password;
    @ManyToOne()
    @JoinColumn(name = "subsidiary_id")
    private SubsidiaryEntity subsidiary;
    @Temporal(TemporalType.DATE)
    private Date deleted;
    @Temporal(TemporalType.DATE)
    private Date created;
    @Temporal(TemporalType.DATE)
    private Date modified;

    public UserEntity() {
    }

    public UserEntity(int dni, String name, String username, String password, SubsidiaryEntity subsidiary, Date deleted, Date created, Date modified) {
        this.dni = dni;
        this.name = name;
        this.username = username;
        this.password = password;
        this.subsidiary = subsidiary;
        this.deleted = deleted;
        this.created = created;
        this.modified = modified;
    }

    public int getDni() {
        return dni;
    }

    public String getName() {
        return name;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public SubsidiaryEntity getSubsidiary() {
        return subsidiary;
    }

    public Date getDeleted() {
        return deleted;
    }

    public Date getCreated() {
        return created;
    }

    public Date getModified() {
        return modified;
    }

    public void setDni(int dni) {
        this.dni = dni;
    }

    public void setDeleted(Date deleted) {
        this.deleted = deleted;
    }

    public void setModified(Date modified) {
        this.modified = modified;
    }
}
