package com.folcademy.myminimarket.models.entities;

import javax.persistence.*;
import java.util.Date;

@Entity(name = "subsidiaries")
public class SubsidiaryEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;
    private String name;
    private String address;
    private String city;
    @Temporal(TemporalType.DATE)
    private Date deleted;
    @Temporal(TemporalType.DATE)
    private Date created;
    @Temporal(TemporalType.DATE)
    private Date modified;

    public SubsidiaryEntity() {
    }

    public SubsidiaryEntity(String name, String address, String city, Date deleted, Date created, Date modified) {
        this.name = name;
        this.address = address;
        this.city = city;
        this.deleted = deleted;
        this.created = created;
        this.modified = modified;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getAddress() {
        return address;
    }

    public String getCity() {
        return city;
    }

    public Date getDeleted() {
        return deleted;
    }

    public Date getCreated() {
        return created;
    }

    public Date getModified() {
        return modified;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setDeleted(Date deleted) {
        this.deleted = deleted;
    }

    public void setModified(Date modified) {
        this.modified = modified;
    }
}
