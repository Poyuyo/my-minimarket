package com.folcademy.myminimarket.models.dto;

public class SubsidiaryDTO {

    private int id;
    private String name;
    private String address;
    private String city;

    public SubsidiaryDTO() {
    }

    public SubsidiaryDTO(int id, String name, String address, String city) {
        this.id = id;
        this.name = name;
        this.address = address;
        this.city = city;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getAddress() {
        return address;
    }

    public String getCity() {
        return city;
    }
}
