package com.folcademy.myminimarket.models.mappers;

import com.folcademy.myminimarket.models.dto.*;
import com.folcademy.myminimarket.models.entities.BrandEntity;
import com.folcademy.myminimarket.models.entities.CategoryEntity;
import com.folcademy.myminimarket.models.entities.ProductEntity;
import org.springframework.stereotype.Component;

import java.util.Date;

@Component
public class ProductMapper {

    public ProductDTO mapProductEntityToProductDto(ProductEntity productEntity, CategoryDTO categoryDTO, BrandDTO brandDTO) {
        return new ProductDTO(productEntity.getId(),
                productEntity.getName(),
                productEntity.getObservations(),
                productEntity.getMeasure_unit().toString(),
                productEntity.getUnit_amount(),
                categoryDTO,
                brandDTO,
                productEntity.getExpiration_data());
    }

    public ProductDetailDTO mapProductEntityToProductDetailDto(ProductEntity productEntity) {
        return new ProductDetailDTO(productEntity.getName(),
                productEntity.getObservations(),
                productEntity.getMeasure_unit(),
                productEntity.getUnit_amount(),
                productEntity.getCategory(),
                productEntity.getBrand(),
                productEntity.getExpiration_data(),
                productEntity.getDeleted(),
                productEntity.getCreated(),
                productEntity.getModified());
    }

    public ProductEntity mapProductNewDtoToProductEntity(ProductNewDTO productNewDTO, CategoryEntity categoryEntity, BrandEntity brandEntity) {
        return new ProductEntity(productNewDTO.getName(),
                productNewDTO.getObservations(),
                productNewDTO.getMeasure_unit(),
                productNewDTO.getUnit_amount(),
                categoryEntity,
                brandEntity,
                productNewDTO.getExpiration_data(),
                null,
                new Date(),
                new Date());
    }

    public ProductEntity mapProductEditDtoToProductEntity(ProductEditDTO productEditDTO, CategoryEntity categoryEntity, BrandEntity brandEntity, Date deleted, Date created) {
        return new ProductEntity(productEditDTO.getName(),
                productEditDTO.getObservations(),
                productEditDTO.getMeasure_unit(),
                productEditDTO.getUnit_amount(),
                categoryEntity,
                brandEntity,
                productEditDTO.getExpiration_data(),
                deleted,
                created,
                new Date());
    }
}
