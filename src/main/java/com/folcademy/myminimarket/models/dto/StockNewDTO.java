package com.folcademy.myminimarket.models.dto;

public class StockNewDTO {
    private int amount;
    private String observations;
    private int product_id;
    private int subsidiary_id;
    private int user_id;

    public StockNewDTO() {
    }

    public StockNewDTO(int amount, String observations, int product_id, int subsidiary_id, int user_id) {
        this.amount = amount;
        this.observations = observations;
        this.product_id = product_id;
        this.subsidiary_id = subsidiary_id;
        this.user_id = user_id;
    }

    public int getAmount() {
        return amount;
    }

    public String getObservations() {
        return observations;
    }

    public int getProduct_id() {
        return product_id;
    }

    public int getSubsidiary_id() {
        return subsidiary_id;
    }

    public int getUser_id() {
        return user_id;
    }
}
