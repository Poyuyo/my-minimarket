package com.folcademy.myminimarket.controllers;

import com.folcademy.myminimarket.models.dto.CategoryDTO;
import com.folcademy.myminimarket.models.dto.CategoryDetailDTO;
import com.folcademy.myminimarket.models.dto.CategoryNewDTO;
import com.folcademy.myminimarket.services.CategoryService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/categories")
public class CategoryController {

    private final CategoryService categoryService;

    public CategoryController(CategoryService categoryService) {
        this.categoryService = categoryService;
    }

    @GetMapping("")
    public ResponseEntity<List<CategoryDTO>> getCategories (){
        return new ResponseEntity<>(categoryService.getCategories(), HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<CategoryDetailDTO> getCategory (@PathVariable int id){
        return new ResponseEntity<>(categoryService.getCategory(id), HttpStatus.OK);
    }


    @PostMapping("")
    public ResponseEntity<String> createCategory(@RequestBody CategoryNewDTO categoryNewDTO){
        return categoryService.createCategory(categoryNewDTO);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<String> deleteCategory(@PathVariable int id){
        return categoryService.deleteCategory(id);
    }

    @PutMapping("")
    public ResponseEntity<String> editCategory(@RequestBody CategoryDTO categoryDTO){
        return categoryService.editCategory(categoryDTO);
    }
}
