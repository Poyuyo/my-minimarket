package com.folcademy.myminimarket.models.dto;

public class StockDTO {

    private int id;
    private int amount;
    private String observations;
    /*private String productName;
    private String subsidiaryName;
    private String userName;*/
    private ProductDTO product;
    private SubsidiaryDTO subsidiary;
    private UserMinDTO user;

    public StockDTO() {
    }

    public StockDTO(int id, int amount, String observations, ProductDTO product, SubsidiaryDTO subsidiary, UserMinDTO user) {
        this.id = id;
        this.amount = amount;
        this.observations = observations;
        this.product = product;
        this.subsidiary = subsidiary;
        this.user = user;
    }

    public int getId() {
        return id;
    }

    public int getAmount() {
        return amount;
    }

    public String getObservations() {
        return observations;
    }

    public ProductDTO getProduct() {
        return product;
    }

    public SubsidiaryDTO getSubsidiary() {
        return subsidiary;
    }

    public UserMinDTO getUser() {
        return user;
    }
}
