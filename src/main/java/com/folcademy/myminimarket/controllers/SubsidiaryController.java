package com.folcademy.myminimarket.controllers;

import com.folcademy.myminimarket.models.dto.*;
import com.folcademy.myminimarket.services.SubsidiaryService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/subsidiaries")
public class SubsidiaryController {

    private final SubsidiaryService subsidiaryService;

    public SubsidiaryController(SubsidiaryService subsidiaryService) {
        this.subsidiaryService = subsidiaryService;
    }

    @GetMapping("")
    public ResponseEntity<List<SubsidiaryDTO>> getSubsidiaries() {
        return new ResponseEntity<>(subsidiaryService.getSubsiadiaries(), HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<SubsidiaryDetailDTO> getSubsidiary(@PathVariable int id) {
        return new ResponseEntity<>(subsidiaryService.getSubsidiaries(id), HttpStatus.OK);
    }


    @PostMapping("")
    public ResponseEntity<String> createSubsidiary(@RequestBody SubsidiaryNewDTO subsidiaryNewDTO) {
        return subsidiaryService.createSubsidiary(subsidiaryNewDTO);
    }

   @DeleteMapping("/{id}")
    public ResponseEntity<String> deleteSubsidiary(@PathVariable int id) {
        return subsidiaryService.deleteSubsidiary(id);
    }

    @PutMapping("")
    public ResponseEntity<String> editSubsidiary(@RequestBody SubsidiaryEditDTO subsidiaryEditDTO) {
        return subsidiaryService.editSubsidiary(subsidiaryEditDTO);
    }
}
