package com.folcademy.myminimarket.models.dto;


import java.util.List;

public class SaleNewDTO {

    private String state;
    private int subsidiary_id;
    private int user_id;
    private List<SaleItemNewDTO> saleItems;

    public SaleNewDTO() {
    }

    public SaleNewDTO(String state, int subsidiary_id, int user_id, List<SaleItemNewDTO> saleItems) {
        this.state = state;
        this.subsidiary_id = subsidiary_id;
        this.user_id = user_id;
        this.saleItems = saleItems;
    }

    public String getState() {
        return state;
    }

    public int getSubsidiary_id() {
        return subsidiary_id;
    }

    public int getUser_id() {
        return user_id;
    }

    public List<SaleItemNewDTO> getSaleItems() {
        return saleItems;
    }
}
