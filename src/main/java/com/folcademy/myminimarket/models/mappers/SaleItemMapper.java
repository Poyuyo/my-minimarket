package com.folcademy.myminimarket.models.mappers;

import com.folcademy.myminimarket.models.dto.ProductDTO;
import com.folcademy.myminimarket.models.dto.SaleItemDTO;
import com.folcademy.myminimarket.models.dto.SaleItemNewDTO;
import com.folcademy.myminimarket.models.entities.ProductEntity;
import com.folcademy.myminimarket.models.entities.SaleEntity;
import com.folcademy.myminimarket.models.entities.SaleItemEntity;
import org.springframework.stereotype.Component;

import java.util.Date;

@Component
public class SaleItemMapper {

    public SaleItemDTO mapSaleItemEntityToSaleItemDTO(SaleItemEntity saleItemEntity, ProductDTO productDTO) {
        return new SaleItemDTO(saleItemEntity.getId(), saleItemEntity.getAmount(), saleItemEntity.getSale_id(), productDTO);
    }

    public SaleItemEntity mapSaleItemNewDTOToSaleItemEntity(SaleItemNewDTO saleItemNewDTO, ProductEntity productEntity) {
        return new SaleItemEntity(saleItemNewDTO.getAmount(),
                saleItemNewDTO.getSale_id(),
                productEntity,
                null,
                new Date(),
                new Date());
    }
}
