package com.folcademy.myminimarket.models.repositories;

import com.folcademy.myminimarket.models.entities.ProductEntity;
import com.folcademy.myminimarket.models.entities.StockEntity;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;

public interface StockRepository extends CrudRepository<StockEntity, String> {
    List<StockEntity> findAll();

    Optional<StockEntity> findById(int id);

    List<StockEntity> findByProduct(ProductEntity productEntity);

    List<StockEntity> findAllByProduct(ProductEntity product);
}
