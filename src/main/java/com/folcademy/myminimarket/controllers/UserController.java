package com.folcademy.myminimarket.controllers;


import com.folcademy.myminimarket.exceptions.NotAuthorizedException;
import com.folcademy.myminimarket.models.dto.*;
import com.folcademy.myminimarket.security.JwtUtil;
import com.folcademy.myminimarket.services.JpaUserDetailService;
import com.folcademy.myminimarket.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.web.bind.annotation.*;


import java.util.List;

@RestController
@RequestMapping("/users")
public class UserController {

    @Autowired
    private JpaUserDetailService jpaUserDetailService;
    @Autowired
    private JwtUtil jwtUtil;
    @Autowired
    private AuthenticationManager authenticationManager;

    private final UserService userService;

    public UserController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping("")
    public ResponseEntity<List<UserDTO>> getUsers() {
        return new ResponseEntity<>(userService.getUsers(), HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<UserDetailDTO> getUser(@PathVariable int id) {
        return new ResponseEntity<>(userService.getUser(id), HttpStatus.OK);
    }

    @PostMapping("")
    public ResponseEntity<String> createUser(@RequestBody UserNewDTO userNewDTO) {
        return userService.createUser(userNewDTO);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<String> deleteSubsidiary(@PathVariable int id) {
        return userService.deleteUser(id);
    }

    @PutMapping("")
    public ResponseEntity<String> editUser(@RequestBody UserNewDTO userNewDTO) {
        return userService.editUser(userNewDTO);
    }

    @PostMapping("/login")
    public ResponseEntity<AuthenticationResponse> createToken(@RequestBody AuthenticateRequest authenticateRequest) {
        UserDetails userDetails;
        try {
            userDetails = jpaUserDetailService.loadUserByUsername(authenticateRequest.getUsername());
        } catch (UsernameNotFoundException e) {
            throw new NotAuthorizedException("El usuario o la contraseña son incorrectos");
        }

        authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(authenticateRequest.getUsername(), authenticateRequest.getPassword())
        );

        String jwt = "Bearer " + jwtUtil.generateToken(userDetails);
        AuthenticationResponse response = new AuthenticationResponse(jwt);
        return ResponseEntity.ok(response);
    }

    @PostMapping("/logout")
    public String logout(){
        return "Logout";
    }
}
