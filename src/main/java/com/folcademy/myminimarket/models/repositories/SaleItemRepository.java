package com.folcademy.myminimarket.models.repositories;

import com.folcademy.myminimarket.models.entities.SaleItemEntity;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface SaleItemRepository extends CrudRepository<SaleItemEntity, String> {
    Optional<SaleItemEntity> findById(int id);
}
