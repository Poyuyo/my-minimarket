package com.folcademy.myminimarket.models.dto;

public class ReportStockDTO {
    private ProductDTO product;
    private int stock;

    public ReportStockDTO() {
    }

    public ReportStockDTO(ProductDTO product, int stock) {
        this.product = product;
        this.stock = stock;
    }

    public ProductDTO getProduct() {
        return product;
    }

    public int getStock() {
        return stock;
    }
}
