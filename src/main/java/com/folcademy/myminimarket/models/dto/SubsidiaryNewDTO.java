package com.folcademy.myminimarket.models.dto;

import java.util.Date;

public class SubsidiaryNewDTO {
    private String name;
    private String address;
    private String city;

    public SubsidiaryNewDTO() {
    }

    public SubsidiaryNewDTO(String name, String address, String city) {
        this.name = name;
        this.address = address;
        this.city = city;
    }

    public String getName() {
        return name;
    }

    public String getAddress() {
        return address;
    }

    public String getCity() {
        return city;
    }
}
