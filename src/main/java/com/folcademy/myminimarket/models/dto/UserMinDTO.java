package com.folcademy.myminimarket.models.dto;

public class UserMinDTO {
    private int dni;
    private String name;

    public UserMinDTO() {
    }

    public UserMinDTO(int dni, String name) {
        this.dni = dni;
        this.name = name;
    }

    public int getDni() {
        return dni;
    }

    public String getName() {
        return name;
    }
}
