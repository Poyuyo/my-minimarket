package com.folcademy.myminimarket.controllers;

import com.folcademy.myminimarket.models.dto.*;

import com.folcademy.myminimarket.services.SaleService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/sales")
public class SaleController {

    private final SaleService saleService;

    public SaleController(SaleService saleService) {
        this.saleService = saleService;
    }

    @GetMapping("")
    public ResponseEntity<List<SaleDTO>> getSales() {
        return new ResponseEntity<>(saleService.getSales(), HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<SaleDetailDTO> getSale(@PathVariable int id) {
        return new ResponseEntity<>(saleService.getSale(id), HttpStatus.OK);
    }

    @PostMapping("")
    public ResponseEntity<String> createSale(@RequestBody SaleNewDTO saleNewDTO) {
        return saleService.createSale(saleNewDTO);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<String> deleteSale(@PathVariable int id) {
        return saleService.deleteSale(id);
    }

    /*@PutMapping("")
    public ResponseEntity<String> editSale(@RequestBody SaleEditDTO saleEditDTO){
        return saleService.editSale(saleEditDTO);
    }*/

}
