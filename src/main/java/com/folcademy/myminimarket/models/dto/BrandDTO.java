package com.folcademy.myminimarket.models.dto;

public class BrandDTO {

    private int id;
    private String name;

    public BrandDTO() {
    }

    public BrandDTO(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }
}
