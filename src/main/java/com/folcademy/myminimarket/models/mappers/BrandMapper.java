package com.folcademy.myminimarket.models.mappers;

import com.folcademy.myminimarket.models.dto.BrandDTO;
import com.folcademy.myminimarket.models.entities.BrandEntity;
import org.springframework.stereotype.Component;

@Component
public class BrandMapper {

    public BrandDTO mapBrandEntityToBrandDTO(BrandEntity brandEntity){
        return new BrandDTO(brandEntity.getId(), brandEntity.getName());
    }
}
