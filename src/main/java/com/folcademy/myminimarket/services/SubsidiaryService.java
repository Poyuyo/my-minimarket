package com.folcademy.myminimarket.services;

import com.folcademy.myminimarket.exceptions.NotFoundException;
import com.folcademy.myminimarket.models.dto.*;
import com.folcademy.myminimarket.models.entities.SubsidiaryEntity;
import com.folcademy.myminimarket.models.mappers.SubsidiaryMapper;
import com.folcademy.myminimarket.models.repositories.SubsidiaryRepository;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
public class SubsidiaryService {
    private final SubsidiaryRepository subsidiaryRepository;
    private final SubsidiaryMapper subsidiaryMapper;

    public SubsidiaryService(SubsidiaryRepository subsidiaryRepository, SubsidiaryMapper subsidiaryMapper) {
        this.subsidiaryRepository = subsidiaryRepository;
        this.subsidiaryMapper = subsidiaryMapper;
    }

    public List<SubsidiaryDTO> getSubsiadiaries() {
        List<SubsidiaryEntity> subsidiaryEntities = subsidiaryRepository.findAll();
        List<SubsidiaryDTO> subsidiaryDTOS = new ArrayList<>();
        for (SubsidiaryEntity subsidiaryEntity : subsidiaryEntities) {
            if (subsidiaryEntity.getDeleted() == null) {
                subsidiaryDTOS.add(subsidiaryMapper.mapSubsidiaryEntityToSubsidiaryDTO(subsidiaryEntity));
            }
        }
        return subsidiaryDTOS;
    }

    public SubsidiaryDetailDTO getSubsidiaries(int id) {
        Optional<SubsidiaryEntity> optionalSubsidiaryEntity = subsidiaryRepository.findById(id);

        if (optionalSubsidiaryEntity.isPresent()) {
            SubsidiaryEntity subsidiaryEntity = optionalSubsidiaryEntity.get();
            SubsidiaryDetailDTO subsidiaryDetailDTO = subsidiaryMapper.mapSubsidiaryEntityToSubsidiaryDetailDTO(subsidiaryEntity);

            return subsidiaryDetailDTO;
        } else {
            throw new NotFoundException("No existe subsidiary");
        }
    }

    public ResponseEntity<String> createSubsidiary(SubsidiaryNewDTO subsidiaryNewDTO) {
        SubsidiaryEntity subsidiaryEntity = subsidiaryMapper.mapSubsidiaryNewDTOToSubsidiaryEntity(subsidiaryNewDTO);
        subsidiaryRepository.save(subsidiaryEntity);
        return new ResponseEntity<>("Created", HttpStatus.OK);
    }

    public ResponseEntity<String> deleteSubsidiary(int id) {
        Optional<SubsidiaryEntity> optionalSubsidiaryEntity = subsidiaryRepository.findById(id);

        if (optionalSubsidiaryEntity.isPresent()) {
            SubsidiaryEntity subsidiaryEntity = optionalSubsidiaryEntity.get();
            subsidiaryEntity.setDeleted(new Date());

            subsidiaryRepository.save(subsidiaryEntity);

            return new ResponseEntity<>("Deleted", HttpStatus.OK);
        } else {
            throw new NotFoundException("No se encontró subsidiary");

        }
    }

    public ResponseEntity<String> editSubsidiary(SubsidiaryEditDTO subsidiaryEditDTO) {
        Optional<SubsidiaryEntity> optionalSubsidiaryEntity = subsidiaryRepository.findById(subsidiaryEditDTO.getId());

        if (optionalSubsidiaryEntity.isPresent()){
            SubsidiaryEntity subsidiaryEntity = subsidiaryMapper.mapSubsidiaryEditDTOToSubsidiaryEntity(subsidiaryEditDTO,
                    optionalSubsidiaryEntity.get().getDeleted(),
                    optionalSubsidiaryEntity.get().getCreated());
            subsidiaryEntity.setId(subsidiaryEditDTO.getId());

            subsidiaryRepository.save(subsidiaryEntity);
            return new ResponseEntity<>("Modified", HttpStatus.OK);
        }else{
            throw new NotFoundException("No se encontró subsidiary");
        }
    }
}
