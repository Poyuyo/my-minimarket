package com.folcademy.myminimarket.services;

import com.folcademy.myminimarket.models.dto.BrandDTO;
import com.folcademy.myminimarket.models.dto.CategoryDTO;
import com.folcademy.myminimarket.models.dto.ReportStockDTO;
import com.folcademy.myminimarket.models.dto.SaleDTO;
import com.folcademy.myminimarket.models.entities.ProductEntity;
import com.folcademy.myminimarket.models.entities.SaleEntity;
import com.folcademy.myminimarket.models.entities.StockEntity;
import com.folcademy.myminimarket.models.mappers.BrandMapper;
import com.folcademy.myminimarket.models.mappers.CategoryMapper;
import com.folcademy.myminimarket.models.mappers.ProductMapper;
import com.folcademy.myminimarket.models.repositories.*;
import org.springframework.stereotype.Service;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class ReportService {

    private final ProductRepository productRepository;
    private final StockRepository stockRepository;
    private final ProductMapper productMapper;
    private final CategoryMapper categoryMapper;
    private final CategoryRepository categoryRepository;
    private final BrandMapper brandMapper;
    private final BrandRepository brandRepository;
    private final ReportRepository reportRepository;

    public ReportService(ProductRepository productRepository,
                         StockRepository stockRepository,
                         ProductMapper productMapper,
                         CategoryMapper categoryMapper,
                         CategoryRepository categoryRepository,
                         BrandMapper brandMapper,
                         BrandRepository brandRepository,
                         ReportRepository reportRepository) {
        this.productRepository = productRepository;
        this.stockRepository = stockRepository;
        this.productMapper = productMapper;
        this.categoryMapper = categoryMapper;
        this.categoryRepository = categoryRepository;
        this.brandMapper = brandMapper;
        this.brandRepository = brandRepository;
        this.reportRepository = reportRepository;
    }

    public List<ReportStockDTO> getStocks() {
        List<ReportStockDTO> reportStockDTOList = new ArrayList<>();
        List<ProductEntity> productEntities = productRepository.findAll();

        for (ProductEntity productEntity : productEntities) {
            List<StockEntity> stockEntities = stockRepository.findByProduct(productEntity);
            int stock = 0;
            CategoryDTO categoryDTO = categoryMapper.mapCategoryEntityToCategoryDto(productEntity.getCategory());
            BrandDTO brandDTO = brandMapper.mapBrandEntityToBrandDTO(productEntity.getBrand());

            for (StockEntity stockEntity : stockEntities) {
                stock += stockEntity.getAmount();
            }
            reportStockDTOList.add(new ReportStockDTO(productMapper.mapProductEntityToProductDto(productEntity, categoryDTO, brandDTO), stock));
        }

        return reportStockDTOList;
    }


    public List<SaleEntity> getSales(String dateFrom, String dateTo) throws ParseException {

        return reportRepository.findAllByCreatedBetween(parseFecha(dateFrom), parseFecha(dateTo));

    }

    private Date parseFecha(String fecha) throws ParseException {
        SimpleDateFormat formato = new SimpleDateFormat("yyyy-MM-dd");
        Date fechaDate = null;
        fechaDate = formato.parse(fecha);


        return fechaDate;
    }
}
