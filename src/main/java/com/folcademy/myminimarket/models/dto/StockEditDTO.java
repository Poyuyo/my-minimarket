package com.folcademy.myminimarket.models.dto;

import java.util.Date;

public class StockEditDTO {

    private int id;
    private int amount;
    private String observations;
    private int product_id;
    private int subsidiary_id;
    private int user_id;
    private Date deleted;
    private Date created;
    private Date modified;


    public StockEditDTO() {
    }

    public StockEditDTO(int id, int amount, String observations, int product_id, int subsidiary_id, int user_id, Date deleted, Date created, Date modified) {
        this.id = id;
        this.amount = amount;
        this.observations = observations;
        this.product_id = product_id;
        this.subsidiary_id = subsidiary_id;
        this.user_id = user_id;
        this.deleted = deleted;
        this.created = created;
        this.modified = modified;
    }

    public int getId() {
        return id;
    }

    public int getAmount() {
        return amount;
    }

    public String getObservations() {
        return observations;
    }

    public int getProduct_id() {
        return product_id;
    }

    public int getSubsidiary_id() {
        return subsidiary_id;
    }

    public int getUser_id() {
        return user_id;
    }

    public Date getDeleted() {
        return deleted;
    }

    public Date getCreated() {
        return created;
    }

    public Date getModified() {
        return modified;
    }
}
