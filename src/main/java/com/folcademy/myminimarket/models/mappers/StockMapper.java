package com.folcademy.myminimarket.models.mappers;

import com.folcademy.myminimarket.models.dto.*;
import com.folcademy.myminimarket.models.entities.ProductEntity;
import com.folcademy.myminimarket.models.entities.StockEntity;
import com.folcademy.myminimarket.models.entities.SubsidiaryEntity;
import com.folcademy.myminimarket.models.entities.UserEntity;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Date;

@Component
public class StockMapper {
    public StockDTO mapStockEntityToStockDTO(StockEntity stockEntity, ArrayList params) {
        return new StockDTO(stockEntity.getId(),
                stockEntity.getAmount(),
                stockEntity.getObservations(),
                (ProductDTO) params.get(0),
                (SubsidiaryDTO) params.get(1),
                (UserMinDTO) params.get(2));
    }

    public StockDetailDTO mapStockEntityToStockDetailDTO(ProductDTO productDTO, int stock) {
        return new StockDetailDTO(productDTO,stock);
    }

    public StockEntity mapStockDTOToStockEntity(StockNewDTO stockNewDTO,
                                                ProductEntity productEntity,
                                                SubsidiaryEntity subsidiaryEntity,
                                                UserEntity userEntity) {
        return new StockEntity(stockNewDTO.getAmount(),
                stockNewDTO.getObservations(),
                productEntity,
                subsidiaryEntity,
                userEntity,
                null,
                new Date(),
                new Date());
    }

    public StockEntity mapStockEditDTOToStockEntity(StockEditDTO stockEditDTO,
                                                    ProductEntity productEntity,
                                                    SubsidiaryEntity subsidiaryEntity,
                                                    UserEntity userEntity, Date deleted, Date created) {
        return new StockEntity(stockEditDTO.getAmount(),
                stockEditDTO.getObservations(),
                productEntity,
                subsidiaryEntity,
                userEntity,
                deleted,
                created,
                new Date()
        );
    }
}
