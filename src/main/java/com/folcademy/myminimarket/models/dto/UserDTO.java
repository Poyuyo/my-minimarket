package com.folcademy.myminimarket.models.dto;

public class UserDTO {
    private int dni;
    private String name;
    private SubsidiaryDTO subsidiary;

    public UserDTO(int dni, String name, SubsidiaryDTO subsidiary) {
        this.dni = dni;
        this.name = name;
        this.subsidiary = subsidiary;
    }

    public int getDni() {
        return dni;
    }

    public String getName() {
        return name;
    }

    public SubsidiaryDTO getSubsidiary() {
        return subsidiary;
    }
}
