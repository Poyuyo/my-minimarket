package com.folcademy.myminimarket.services;

import com.folcademy.myminimarket.exceptions.BadRequestException;
import com.folcademy.myminimarket.exceptions.NotFoundException;
import com.folcademy.myminimarket.models.dto.*;
import com.folcademy.myminimarket.models.entities.*;
import com.folcademy.myminimarket.models.mappers.*;
import com.folcademy.myminimarket.models.repositories.*;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
public class SaleService {

    private final SaleRepository saleRepository;
    private final SaleMapper saleMapper;
    private final SubsidiaryMapper subsidiaryMapper;
    private final UserMapper userMapper;
    private final SaleItemMapper saleItemMapper;
    private final ProductMapper productMapper;
    private final CategoryMapper categoryMapper;
    private final BrandMapper brandMapper;
    private final SubsidiaryRepository subsidiaryRepository;
    private final UserRepository userRepository;
    private final ProductRepository productRepository;
    private final StockService stockService;
    private final SaleItemRepository saleItemRepository;

    public SaleService(SaleRepository saleRepository,
                       SaleMapper saleMapper,
                       SubsidiaryMapper subsidiaryMapper,
                       UserMapper userMapper,
                       SaleItemMapper saleItemMapper,
                       ProductMapper productMapper,
                       CategoryMapper categoryMapper,
                       BrandMapper brandMapper,
                       SubsidiaryRepository subsidiaryRepository,
                       UserRepository userRepository,
                       ProductRepository productRepository,
                       StockService stockService, SaleItemRepository saleItemRepository) {
        this.saleRepository = saleRepository;
        this.saleMapper = saleMapper;
        this.subsidiaryMapper = subsidiaryMapper;
        this.userMapper = userMapper;
        this.saleItemMapper = saleItemMapper;
        this.productMapper = productMapper;
        this.categoryMapper = categoryMapper;
        this.brandMapper = brandMapper;
        this.subsidiaryRepository = subsidiaryRepository;
        this.userRepository = userRepository;
        this.productRepository = productRepository;
        this.stockService = stockService;
        this.saleItemRepository = saleItemRepository;
    }

    public List<SaleDTO> getSales() {
        List<SaleEntity> saleEntities = saleRepository.findAll();
        List<SaleDTO> saleDTOS = new ArrayList<>();

        for (SaleEntity saleEntity : saleEntities) {
            if (saleEntity.getDeleted() == null) {
                SubsidiaryDTO subsidiaryDTO = subsidiaryMapper.mapSubsidiaryEntityToSubsidiaryDTO(saleEntity.getSubsidiary());
                UserMinDTO userMinDTO = userMapper.mapUserEntityToUserMinDTO(saleEntity.getUser());
                List<SaleItemDTO> saleItemDTOList = getSaleItemsDTO(saleEntity.getSaleItems());
                BigDecimal totalAmount = getTotalAmount(saleEntity.getSaleItems());

                saleDTOS.add(saleMapper.mapSaleEntityToSaleDTO(saleEntity, subsidiaryDTO, userMinDTO, saleItemDTOList, totalAmount));
            }
        }

        return saleDTOS;
    }


    public SaleDetailDTO getSale(int id) {
        Optional<SaleEntity> optionalSaleEntity = saleRepository.findById(id);

        if (optionalSaleEntity.isPresent()) {
            SaleEntity saleEntity = optionalSaleEntity.get();
            SubsidiaryDTO subsidiaryDTO = subsidiaryMapper.mapSubsidiaryEntityToSubsidiaryDTO(saleEntity.getSubsidiary());
            UserMinDTO userMinDTO = userMapper.mapUserEntityToUserMinDTO(saleEntity.getUser());
            List<SaleItemDTO> saleItemDTOList = getSaleItemsDTO(saleEntity.getSaleItems());
            BigDecimal totalAmount = getTotalAmount(saleEntity.getSaleItems());

            SaleDetailDTO saleDetailDTO = saleMapper.mapSaleEntityToSaleDetailDTO(saleEntity, subsidiaryDTO, userMinDTO, saleItemDTOList, totalAmount);

            return saleDetailDTO;

        } else {
            throw new NotFoundException("No se encontró Sale");
        }
    }

    public ResponseEntity<String> deleteSale(int id) {
        Optional<SaleEntity> optionalSaleEntity = saleRepository.findById(id);

        if (optionalSaleEntity.isPresent()) {
            SaleEntity saleEntity = optionalSaleEntity.get();
            saleEntity.setDeleted(new Date());
            saleRepository.save(saleEntity);

            return new ResponseEntity<>("Deleted", HttpStatus.OK);
        } else {
            throw new NotFoundException("No se encontró Sale");
        }
    }

    public ResponseEntity<String> createSale(SaleNewDTO saleNewDTO) {
        List<SaleItemNewDTO> saleItemNewDTOS = saleNewDTO.getSaleItems();
        List<SaleItemEntity> saleItemEntities = new ArrayList<>();

        if(userRepository.findByDni(saleNewDTO.getUser_id()).isEmpty()){
            throw new BadRequestException("No existe User");
        }

        if(subsidiaryRepository.findById(saleNewDTO.getSubsidiary_id()).isEmpty()){
            throw new BadRequestException("No existe Subsidiary");
        }

        for (SaleItemNewDTO saleItemNewDTO : saleItemNewDTOS) {

            if (productRepository.existsById(saleItemNewDTO.getProduct_id())) {
                if (stockService.getStock(saleItemNewDTO.getProduct_id()).getStock() > saleItemNewDTO.getAmount()) {
                    ProductEntity productEntity = productRepository.findById(saleItemNewDTO.getProduct_id()).get();
                    saleItemEntities.add(saleItemMapper.mapSaleItemNewDTOToSaleItemEntity(saleItemNewDTO, productEntity));
                } else {
                    throw new BadRequestException("No hay stock suficiente para el producto " + saleItemNewDTO.getProduct_id());
                }
            } else {
                throw new RuntimeException("No existe producto con id [" + saleItemNewDTO.getProduct_id() + "]");
            }
        }

        SubsidiaryEntity subsidiaryEntity = subsidiaryRepository.findById(saleNewDTO.getSubsidiary_id()).get();
        UserEntity userEntity = userRepository.findByDni(saleNewDTO.getUser_id()).get();
        SaleEntity saleEntity = saleMapper.mapSaleNewDTOToSaleEntity(saleNewDTO, subsidiaryEntity, userEntity, saleItemEntities);
        updateStock(saleEntity);
        saleRepository.save(saleEntity);

        return new ResponseEntity<>("Created", HttpStatus.OK);
    }


    private void updateStock(SaleEntity saleEntity) {
        for (SaleItemEntity saleItemEntity : saleEntity.getSaleItems()) {
            StockNewDTO stockNewDTO = new StockNewDTO(saleItemEntity.getAmount() * (-1),
                    "",
                    saleItemEntity.getProduct().getId(),
                    saleEntity.getSubsidiary().getId(),
                    saleEntity.getUser().getDni());
            stockService.createStock(stockNewDTO);
        }
    }


    private List<SaleItemDTO> getSaleItemsDTO(List<SaleItemEntity> saleItems) {
        List<SaleItemDTO> saleItemDTOList = new ArrayList<>();
        for (SaleItemEntity saleItemEntity : saleItems) {
            CategoryDTO categoryDTO = categoryMapper.mapCategoryEntityToCategoryDto(saleItemEntity.getProduct().getCategory());
            BrandDTO brandDTO = brandMapper.mapBrandEntityToBrandDTO(saleItemEntity.getProduct().getBrand());
            ProductDTO productDTO = productMapper.mapProductEntityToProductDto(saleItemEntity.getProduct(), categoryDTO, brandDTO);
            saleItemDTOList.add(saleItemMapper.mapSaleItemEntityToSaleItemDTO(saleItemEntity, productDTO));
        }
        return saleItemDTOList;
    }

    private BigDecimal getTotalAmount(List<SaleItemEntity> saleItems) {
        BigDecimal totalAmount = BigDecimal.ZERO;

        for (SaleItemEntity saleItemEntity : saleItems) {
            totalAmount = totalAmount.add(saleItemEntity.getProduct().getUnit_amount().multiply(new BigDecimal(saleItemEntity.getAmount())));
        }

        return totalAmount;
    }

}
