package com.folcademy.myminimarket.models.dto;

import java.util.List;

public class SaleEditDTO {
    private int id;
    private String state;
    private int subsidiary_id;
    private int user_id;
    private List<SaleItemNewDTO> saleItems;

    public SaleEditDTO() {
    }

    public SaleEditDTO(int id, String state, int subsidiary_id, int user_id, List<SaleItemNewDTO> saleItems) {
        this.id = id;
        this.state = state;
        this.subsidiary_id = subsidiary_id;
        this.user_id = user_id;
        this.saleItems = saleItems;
    }

    public int getId() {
        return id;
    }

    public String getState() {
        return state;
    }

    public int getSubsidiary_id() {
        return subsidiary_id;
    }

    public int getUser_id() {
        return user_id;
    }

    public List<SaleItemNewDTO> getSaleItems() {
        return saleItems;
    }
}
