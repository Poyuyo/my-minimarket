package com.folcademy.myminimarket.models.repositories;

import com.folcademy.myminimarket.models.entities.CategoryEntity;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;

public interface CategoryRepository extends CrudRepository<CategoryEntity, String> {

    List<CategoryEntity> findAll();
    Optional<CategoryEntity> findById(int id);
}
