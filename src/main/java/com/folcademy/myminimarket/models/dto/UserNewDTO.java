package com.folcademy.myminimarket.models.dto;

import java.util.Date;

public class UserNewDTO {

    private int dni;
    private String name;
    private String username;
    private String password;
    private int subsidiary_id;


    public UserNewDTO() {
    }

    public UserNewDTO(int dni, String name, String username, String password, int subsidiary_id) {
        this.dni = dni;
        this.name = name;
        this.username = username;
        this.password = password;
        this.subsidiary_id = subsidiary_id;

    }

    public int getDni() {
        return dni;
    }

    public String getName() {
        return name;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public int getSubsidiary_id() {
        return subsidiary_id;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
