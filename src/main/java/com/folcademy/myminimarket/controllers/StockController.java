package com.folcademy.myminimarket.controllers;

import com.folcademy.myminimarket.models.dto.*;
import com.folcademy.myminimarket.services.StockService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/stocks")
public class StockController {

    private final StockService stockService;

    public StockController(StockService stockService) {
        this.stockService = stockService;
    }

    @GetMapping("")
    public ResponseEntity<List<StockDTO>> getStocks() {
        return new ResponseEntity<>(stockService.getStocks(), HttpStatus.OK);
    }

    @GetMapping("/{id_prod}")
    public ResponseEntity<StockDetailDTO> getStock(@PathVariable int id_prod) {
        return new ResponseEntity<>(stockService.getStock(id_prod), HttpStatus.OK);
    }


    @PostMapping("")
    public ResponseEntity<String> createStock(@RequestBody StockNewDTO stockNewDTO) {
        return stockService.createStock(stockNewDTO);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<String> deleteStock(@PathVariable int id) {
        return stockService.deleteStock(id);
    }

    @PutMapping("")
    public ResponseEntity<String> editStock(@RequestBody StockEditDTO stockEditDTO) {
        return stockService.editStock(stockEditDTO);
    }
}
