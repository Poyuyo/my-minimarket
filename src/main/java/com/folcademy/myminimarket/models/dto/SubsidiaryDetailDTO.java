package com.folcademy.myminimarket.models.dto;

import java.util.Date;

public class SubsidiaryDetailDTO {
    private String name;
    private String address;
    private String city;
    private Date deleted;
    private Date created;
    private Date modified;

    public SubsidiaryDetailDTO() {
    }

    public SubsidiaryDetailDTO(String name, String address, String city, Date deleted, Date created, Date modified) {
        this.name = name;
        this.address = address;
        this.city = city;
        this.deleted = deleted;
        this.created = created;
        this.modified = modified;
    }

    public String getName() {
        return name;
    }

    public String getAddress() {
        return address;
    }

    public String getCity() {
        return city;
    }

    public Date getDeleted() {
        return deleted;
    }

    public Date getCreated() {
        return created;
    }

    public Date getModified() {
        return modified;
    }
}
