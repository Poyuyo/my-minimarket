package com.folcademy.myminimarket.services;

import com.folcademy.myminimarket.exceptions.NotFoundException;
import com.folcademy.myminimarket.models.dto.CategoryDTO;
import com.folcademy.myminimarket.models.dto.CategoryDetailDTO;
import com.folcademy.myminimarket.models.dto.CategoryNewDTO;
import com.folcademy.myminimarket.models.entities.CategoryEntity;
import com.folcademy.myminimarket.models.mappers.CategoryMapper;
import com.folcademy.myminimarket.models.repositories.CategoryRepository;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
public class CategoryService {

    private final CategoryRepository categoryRepository;
    private final CategoryMapper categoryMapper;

    public CategoryService(CategoryRepository categoryRepository, CategoryMapper categoryMapper) {
        this.categoryRepository = categoryRepository;
        this.categoryMapper = categoryMapper;
    }


    public List<CategoryDTO> getCategories() {
        List<CategoryEntity> categoryEntities = categoryRepository.findAll();
        List<CategoryDTO> categoryDTOS = new ArrayList<>();

        for (CategoryEntity categoryEntity : categoryEntities) {
            if (categoryEntity.getDeleted() == null) {
                categoryDTOS.add(categoryMapper.mapCategoryEntityToCategoryDto(categoryEntity));
            }
        }

        return categoryDTOS;
    }

    public CategoryDetailDTO getCategory(int id) {
        Optional<CategoryEntity> optionalCategoryEntity = categoryRepository.findById(id);

        if (optionalCategoryEntity.isPresent()) {
            CategoryEntity categoryEntity = optionalCategoryEntity.get();
            CategoryDetailDTO categoryDetailDTO = categoryMapper.mapCategoryEntityToCategoryDetailDto(categoryEntity);
            return categoryDetailDTO;
        } else {
            throw new NotFoundException("No existe categoría");
        }

    }

    public ResponseEntity<String> createCategory(CategoryNewDTO categoryNewDTO) {

        CategoryEntity categoryEntity = categoryMapper.mapCategoryNewDtoToCategoryEntity(categoryNewDTO);
        categoryRepository.save(categoryEntity);

        return new ResponseEntity<>("Created", HttpStatus.OK);
    }

    public ResponseEntity<String> deleteCategory(int id) {
        Optional<CategoryEntity> optionalCategoryEntity = categoryRepository.findById(id);
        if (optionalCategoryEntity.isPresent()) {
            CategoryEntity categoryEntity = optionalCategoryEntity.get();
            //categoryRepository.delete(categoryEntity);
            categoryEntity.setDeleted(new Date());
            categoryRepository.save(categoryEntity);
            return new ResponseEntity<>("Deleted", HttpStatus.OK);
        } else {
            throw new NotFoundException("No existe categoría");
        }
    }

    public ResponseEntity<String> editCategory(CategoryDTO categoryDTO) {
        Optional<CategoryEntity> optionalCategoryEntity = categoryRepository.findById(categoryDTO.getId());

        if (optionalCategoryEntity.isPresent()) {
            CategoryEntity categoryEntity = optionalCategoryEntity.get();
            categoryEntity.setName(categoryDTO.getName());
            categoryEntity.setModified(new Date());
            categoryRepository.save(categoryEntity);

            return new ResponseEntity<>("Modified", HttpStatus.OK);
        } else {
            throw new NotFoundException("No existe categoría");
        }
    }
}
