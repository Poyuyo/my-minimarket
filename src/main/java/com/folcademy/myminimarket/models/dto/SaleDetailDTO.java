package com.folcademy.myminimarket.models.dto;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

public class SaleDetailDTO {


    private String state;
    private SubsidiaryDTO subsidiary;
    private UserMinDTO user;
    List<SaleItemDTO> saleItems;
    private BigDecimal totalAmount;
    private Date deleted;
    private Date created;
    private Date modified;

    public SaleDetailDTO() {
    }

    public SaleDetailDTO(String state, SubsidiaryDTO subsidiary, UserMinDTO user, List<SaleItemDTO> saleItems, BigDecimal totalAmount, Date deleted, Date created, Date modified) {
        this.state = state;
        this.subsidiary = subsidiary;
        this.user = user;
        this.saleItems = saleItems;
        this.totalAmount = totalAmount;
        this.deleted = deleted;
        this.created = created;
        this.modified = modified;
    }

    public String getState() {
        return state;
    }

    public SubsidiaryDTO getSubsidiary() {
        return subsidiary;
    }

    public UserMinDTO getUser() {
        return user;
    }

    public List<SaleItemDTO> getSaleItems() {
        return saleItems;
    }

    public BigDecimal getTotalAmount() {
        return totalAmount;
    }

    public Date getDeleted() {
        return deleted;
    }

    public Date getCreated() {
        return created;
    }

    public Date getModified() {
        return modified;
    }
}
