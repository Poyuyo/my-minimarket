package com.folcademy.myminimarket.models.dto;

import java.util.Date;

public class CategoryDetailDTO {

    private String name;
    private Date created;
    private Date modified;
    private Date deleted;

    public CategoryDetailDTO() {
    }

    public CategoryDetailDTO(String name, Date created, Date modified, Date deleted) {
        this.name = name;
        this.created = created;
        this.modified = modified;
        this.deleted = deleted;
    }

    public String getName() {
        return name;
    }

    public Date getCreated() {
        return created;
    }

    public Date getModified() {
        return modified;
    }

    public Date getDeleted() {
        return deleted;
    }
}
