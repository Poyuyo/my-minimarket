package com.folcademy.myminimarket.models.mappers;

import com.folcademy.myminimarket.models.dto.*;
import com.folcademy.myminimarket.models.entities.SaleEntity;
import com.folcademy.myminimarket.models.entities.SaleItemEntity;
import com.folcademy.myminimarket.models.entities.SubsidiaryEntity;
import com.folcademy.myminimarket.models.entities.UserEntity;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;


@Component
public class SaleMapper {
    public SaleDTO mapSaleEntityToSaleDTO(SaleEntity saleEntity,
                                          SubsidiaryDTO subsidiaryDTO,
                                          UserMinDTO userMinDTO,
                                          List<SaleItemDTO> saleItemDTOList,
                                          BigDecimal totalAmount) {
        return new SaleDTO(saleEntity.getId(),
                saleEntity.getState(),
                subsidiaryDTO,
                userMinDTO,
                saleItemDTOList,
                totalAmount);
    }

    public SaleDetailDTO mapSaleEntityToSaleDetailDTO(SaleEntity saleEntity,
                                                      SubsidiaryDTO subsidiaryDTO,
                                                      UserMinDTO userMinDTO,
                                                      List<SaleItemDTO> saleItemDTOList,
                                                      BigDecimal totalAmount) {
        return new SaleDetailDTO(saleEntity.getState(),
                subsidiaryDTO,
                userMinDTO,
                saleItemDTOList,
                totalAmount,
                saleEntity.getDeleted(),
                saleEntity.getCreated(),
                saleEntity.getModified());
    }

    public SaleEntity mapSaleNewDTOToSaleEntity(SaleNewDTO saleNewDTO,
                                                SubsidiaryEntity subsidiary,
                                                UserEntity user,
                                                List<SaleItemEntity> saleItemEntities) {

        return new SaleEntity(saleNewDTO.getState(),
                subsidiary,
                user,
                saleItemEntities,
                null,
                new Date(),
                new Date());
    }

    public SaleEntity mapSaleEditDTOToSaleEntity(SaleEditDTO saleEditDTO,
                                                 SubsidiaryEntity subsidiary,
                                                 UserEntity user,
                                                 List<SaleItemEntity> saleItemEntities,
                                                 Date deleted,
                                                 Date created){
        return new SaleEntity(saleEditDTO.getState(),
                subsidiary,
                user,
                saleItemEntities,
                deleted,
                created,
                new Date());

    }
}
