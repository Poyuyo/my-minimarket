package com.folcademy.myminimarket.models.dto;

import java.util.Date;

public class SubsidiaryEditDTO {
    private int id;
    private String name;
    private String address;
    private String city;

    public SubsidiaryEditDTO() {
    }

    public SubsidiaryEditDTO(int id, String name, String address, String city, Date deleted, Date created, Date modified) {
        this.id = id;
        this.name = name;
        this.address = address;
        this.city = city;

    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getAddress() {
        return address;
    }

    public String getCity() {
        return city;
    }

}
