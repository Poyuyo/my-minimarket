package com.folcademy.myminimarket.models.dto;

import com.folcademy.myminimarket.models.entities.BrandEntity;
import com.folcademy.myminimarket.models.entities.CategoryEntity;
import com.folcademy.myminimarket.models.enums.MeasureUnit;

import java.math.BigDecimal;
import java.util.Date;

public class ProductDetailDTO {

    private String name;
    private String observations;
    private MeasureUnit measure_unit;
    private BigDecimal unit_amount;
    private CategoryEntity category;
    private BrandEntity brand;
    private Date expiration_data;
    private Date deleted;
    private Date created;
    private Date modified;

    public ProductDetailDTO() {
    }

    public ProductDetailDTO(String name,
                            String observations,
                            MeasureUnit measure_unit,
                            BigDecimal unit_amount,
                            CategoryEntity category,
                            BrandEntity brand,
                            Date expiration_data,
                            Date deleted, Date created,
                            Date modified) {
        this.name = name;
        this.observations = observations;
        this.measure_unit = measure_unit;
        this.unit_amount = unit_amount;
        this.category = category;
        this.brand = brand;
        this.expiration_data = expiration_data;
        this.deleted = deleted;
        this.created = created;
        this.modified = modified;
    }

    public String getName() {
        return name;
    }

    public String getObservations() {
        return observations;
    }

    public MeasureUnit getMeasure_unit() {
        return measure_unit;
    }

    public BigDecimal getUnit_amount() {
        return unit_amount;
    }

    public CategoryEntity getCategory() {
        return category;
    }

    public BrandEntity getBrand() {
        return brand;
    }

    public Date getExpiration_data() {
        return expiration_data;
    }

    public Date getDeleted() {
        return deleted;
    }

    public Date getCreated() {
        return created;
    }

    public Date getModified() {
        return modified;
    }
}
