package com.folcademy.myminimarket.models.dto;

public class SaleItemNewDTO {
    private int amount;
    private int sale_id;
    private Integer product_id;

    public SaleItemNewDTO() {
    }

    public SaleItemNewDTO(int amount, int sale_id, int product_id) {
        this.amount = amount;
        this.sale_id = sale_id;
        this.product_id = product_id;
    }

    public int getAmount() {
        return amount;
    }

    public int getSale_id() {
        return sale_id;
    }

    public Integer getProduct_id() {
        return product_id;
    }
}
