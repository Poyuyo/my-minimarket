package com.folcademy.myminimarket.controllers;

import com.folcademy.myminimarket.models.dto.*;
import com.folcademy.myminimarket.services.ProductService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/products")
public class ProductController {


    private final ProductService productService;

    public ProductController(ProductService productService) {
        this.productService = productService;
    }


    @GetMapping("")
    public ResponseEntity<List<ProductDTO>> getProducts (){
        return new ResponseEntity<>(productService.getProducts(), HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<ProductDetailDTO> getProduct (@PathVariable int id){
        return new ResponseEntity<>(productService.getProduct(id), HttpStatus.OK);
    }


    @PostMapping("")
    public ResponseEntity<String> createProduct(@RequestBody ProductNewDTO productNewDTO){
        return productService.createProduct(productNewDTO);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<String> deleteProduct(@PathVariable int id){
        return productService.deleteProduct(id);
    }

    @PutMapping("")
    public ResponseEntity<String> editProduct(@RequestBody ProductEditDTO productEditDTO){
        return productService.editProduct(productEditDTO);
    }
}
