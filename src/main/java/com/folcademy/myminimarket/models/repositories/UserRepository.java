package com.folcademy.myminimarket.models.repositories;

import com.folcademy.myminimarket.models.entities.SubsidiaryEntity;
import com.folcademy.myminimarket.models.entities.UserEntity;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;

public interface UserRepository extends CrudRepository<UserEntity, String> {
    List<UserEntity> findAll();

    Optional<UserEntity> findByDni(int dni);

    Optional<UserEntity> findByUsername(String username);
}
