package com.folcademy.myminimarket.models.entities;

import javax.persistence.*;
import java.util.Date;

@Entity(name = "sale_items")
public class SaleItemEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;
    private int amount;
    private int sale_id;
    @ManyToOne()
    @JoinColumn(name = "product_id")
    private ProductEntity product;
    @Temporal(TemporalType.DATE)
    private Date deleted;
    @Temporal(TemporalType.DATE)
    private Date created;
    @Temporal(TemporalType.DATE)
    private Date modified;

    public SaleItemEntity() {
    }

    public SaleItemEntity(int amount, int sale_id, ProductEntity product, Date deleted, Date created, Date modified) {
        this.amount = amount;
        this.sale_id = sale_id;
        this.product = product;
        this.deleted = deleted;
        this.created = created;
        this.modified = modified;
    }

    public Integer getId() {
        return id;
    }

    public int getAmount() {
        return amount;
    }

    public int getSale_id() {
        return sale_id;
    }

    public ProductEntity getProduct() {
        return product;
    }

    public Date getDeleted() {
        return deleted;
    }

    public Date getCreated() {
        return created;
    }

    public Date getModified() {
        return modified;
    }
}
