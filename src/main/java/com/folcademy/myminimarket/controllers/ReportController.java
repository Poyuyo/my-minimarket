package com.folcademy.myminimarket.controllers;

import com.folcademy.myminimarket.models.dto.ReportStockDTO;

import com.folcademy.myminimarket.models.dto.SaleDTO;
import com.folcademy.myminimarket.models.entities.SaleEntity;
import com.folcademy.myminimarket.services.ReportService;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.text.ParseException;
import java.util.Date;
import java.util.List;

@RestController
@RequestMapping("/reports")
public class ReportController {
    private final ReportService reportService;

    public ReportController(ReportService reportService) {
        this.reportService = reportService;
    }

    @GetMapping("")
    public ResponseEntity<List<ReportStockDTO>> getStocks() {
        return new ResponseEntity<>(reportService.getStocks(), HttpStatus.OK);
    }

    @GetMapping("/sales")
    public ResponseEntity<List<SaleEntity>> getSales(@RequestParam(name = "dateFrom", defaultValue = "", required = false) String dateFrom,
                                                     @RequestParam(name = "dateTo", defaultValue = "", required = false) String dateTo) throws ParseException {
        return new ResponseEntity<>(reportService.getSales(dateFrom, dateTo), HttpStatus.OK);
    }
}
