package com.folcademy.myminimarket.services;

import com.folcademy.myminimarket.exceptions.BadRequestException;
import com.folcademy.myminimarket.exceptions.NotFoundException;
import com.folcademy.myminimarket.models.dto.SubsidiaryDTO;
import com.folcademy.myminimarket.models.dto.UserDTO;
import com.folcademy.myminimarket.models.dto.UserDetailDTO;
import com.folcademy.myminimarket.models.dto.UserNewDTO;
import com.folcademy.myminimarket.models.entities.SubsidiaryEntity;
import com.folcademy.myminimarket.models.entities.UserEntity;
import com.folcademy.myminimarket.models.mappers.SubsidiaryMapper;
import com.folcademy.myminimarket.models.mappers.UserMapper;
import com.folcademy.myminimarket.models.repositories.SubsidiaryRepository;
import com.folcademy.myminimarket.models.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
public class UserService {
    @Autowired
    private PasswordEncoder passwordEncoder;
    private final UserRepository userRepository;
    private final UserMapper userMapper;
    private final SubsidiaryMapper subsidiaryMapper;
    private final SubsidiaryRepository subsidiaryRepository;

    public UserService(UserRepository userRepository, UserMapper userMapper, SubsidiaryMapper subsidiaryMapper, SubsidiaryRepository subsidiaryRepository) {
        this.userRepository = userRepository;
        this.userMapper = userMapper;
        this.subsidiaryMapper = subsidiaryMapper;
        this.subsidiaryRepository = subsidiaryRepository;
    }


    public List<UserDTO> getUsers() {
        List<UserEntity> userEntities = userRepository.findAll();
        List<UserDTO> userDTOS = new ArrayList<>();
        for (UserEntity userEntity : userEntities) {
            if (userEntity.getDeleted() == null) {
                SubsidiaryDTO subsidiaryDTO = subsidiaryMapper.mapSubsidiaryEntityToSubsidiaryDTO(userEntity.getSubsidiary());
                userDTOS.add(userMapper.mapUserEntityToUserDTO(userEntity, subsidiaryDTO));
            }
        }

        return userDTOS;
    }

    public UserDetailDTO getUser(int dni) {
        Optional<UserEntity> optionalUserEntity = userRepository.findByDni(dni);

        if (optionalUserEntity.isPresent()) {
            UserDetailDTO userDetailDTO = userMapper.mapUserEntityToUserDetailDto(optionalUserEntity.get());
            return userDetailDTO;
        } else {
            throw new NotFoundException("No se ecncontró User");
        }
    }

    public ResponseEntity<String> createUser(UserNewDTO userNewDTO) {
        Optional<UserEntity> optionalUserEntity = userRepository.findByDni(userNewDTO.getDni());

        if (optionalUserEntity.isEmpty()) {

            userNewDTO.setPassword(passwordEncoder.encode(userNewDTO.getPassword()));
            UserEntity userEntity = userMapper.mapUserNewDtoToUserEntity(userNewDTO,
                    findSubsidiary(userNewDTO.getSubsidiary_id()));
            userRepository.save(userEntity);

            return new ResponseEntity<>("Created", HttpStatus.OK);
        } else {
            throw new BadRequestException("Ya existe usuario con ese dni");
        }
    }


    public ResponseEntity<String> deleteUser(int dni) {
        Optional<UserEntity> optionalUserEntity = userRepository.findByDni(dni);

        if (optionalUserEntity.isPresent()) {
            UserEntity userEntity = optionalUserEntity.get();
            userEntity.setDeleted(new Date());

            userRepository.save(userEntity);

            return new ResponseEntity<>("Deleted", HttpStatus.OK);
        } else {
            throw new NotFoundException("No se encontro usuario");
        }
    }

    public ResponseEntity<String> editUser(UserNewDTO userNewDTO) {
        Optional<UserEntity> optionalUserEntity = userRepository.findByDni(userNewDTO.getDni());

        if (optionalUserEntity.isPresent()){
            userNewDTO.setPassword(passwordEncoder.encode(userNewDTO.getPassword()));
            UserEntity userEntity = userMapper.mapUserNewDtoToUserEntity(userNewDTO,
                    findSubsidiary(userNewDTO.getSubsidiary_id()),
                    optionalUserEntity.get().getDeleted(),
                    optionalUserEntity.get().getCreated());


            userRepository.save(userEntity);
            return new ResponseEntity<>("Modified", HttpStatus.OK);
        }else{
            throw new NotFoundException("No se encontró User");
        }
    }

    public SubsidiaryEntity findSubsidiary(int subsidiary_id) {
        Optional<SubsidiaryEntity> optionalSubsidiaryEntity = subsidiaryRepository.findById(subsidiary_id);
        if (optionalSubsidiaryEntity.isPresent()) {
            return optionalSubsidiaryEntity.get();
        } else {
            throw new NotFoundException("No existe Subsidiary");
        }
    }
}
