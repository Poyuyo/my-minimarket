package com.folcademy.myminimarket.models.enums;

public enum MeasureUnit {
    kg, g, m, cm, l, ml
}
