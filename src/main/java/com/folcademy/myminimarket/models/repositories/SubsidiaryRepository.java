package com.folcademy.myminimarket.models.repositories;

import com.folcademy.myminimarket.models.entities.StockEntity;
import com.folcademy.myminimarket.models.entities.SubsidiaryEntity;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;

public interface SubsidiaryRepository extends CrudRepository<SubsidiaryEntity, String> {
    List<SubsidiaryEntity> findAll();

    Optional<SubsidiaryEntity> findById(int id);
}
