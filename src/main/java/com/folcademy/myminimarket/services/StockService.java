package com.folcademy.myminimarket.services;

import com.folcademy.myminimarket.exceptions.NotFoundException;
import com.folcademy.myminimarket.models.dto.*;
import com.folcademy.myminimarket.models.entities.*;
import com.folcademy.myminimarket.models.mappers.*;
import com.folcademy.myminimarket.models.repositories.ProductRepository;
import com.folcademy.myminimarket.models.repositories.StockRepository;
import com.folcademy.myminimarket.models.repositories.SubsidiaryRepository;
import com.folcademy.myminimarket.models.repositories.UserRepository;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
public class StockService {

    private final StockRepository stockRepository;
    private final StockMapper stockMapper;
    private final ProductMapper productMapper;
    private final SubsidiaryMapper subsidiaryMapper;
    private final UserMapper userMapper;
    private final CategoryMapper categoryMapper;
    private final BrandMapper brandMapper;
    private final ProductRepository productRepository;
    private final SubsidiaryRepository subsidiaryRepository;
    private final UserRepository userRepository;

    public StockService(StockRepository stockRepository,
                        StockMapper stockMapper,
                        ProductMapper productMapper,
                        SubsidiaryMapper subsidiaryMapper,
                        UserMapper userMapper,
                        CategoryMapper categoryMapper,
                        BrandMapper brandMapper,
                        ProductRepository productRepository,
                        SubsidiaryRepository subsidiaryRepository,
                        UserRepository userRepository) {
        this.stockRepository = stockRepository;
        this.stockMapper = stockMapper;
        this.productMapper = productMapper;
        this.subsidiaryMapper = subsidiaryMapper;
        this.userMapper = userMapper;
        this.categoryMapper = categoryMapper;
        this.brandMapper = brandMapper;
        this.productRepository = productRepository;
        this.subsidiaryRepository = subsidiaryRepository;
        this.userRepository = userRepository;
    }

    public List<StockDTO> getStocks() {
        List<StockEntity> stockEntities = stockRepository.findAll();
        List<StockDTO> stockDTOS = new ArrayList<>();
        for (StockEntity stockEntity : stockEntities) {
            if (stockEntity.getDeleted() == null) {
                ArrayList<Object> params = getStocksDTOParams(stockEntity);
                stockDTOS.add(stockMapper.mapStockEntityToStockDTO(stockEntity,
                        params));
            }
        }
        return stockDTOS;
    }

    public StockDetailDTO getStock(int id) {
        Optional<ProductEntity> optionalProductEntity = productRepository.findById(id);

        if (optionalProductEntity.isPresent()) {
            ProductEntity productEntity = optionalProductEntity.get();
            List<StockEntity> stockEntities = stockRepository.findAllByProduct(productEntity);
            Integer stock = 0;
            for (StockEntity stockEntity : stockEntities) {
                stock += stockEntity.getAmount();
            }

            CategoryDTO categoryDTO = categoryMapper.mapCategoryEntityToCategoryDto(productEntity.getCategory());
            BrandDTO brandDTO = brandMapper.mapBrandEntityToBrandDTO(productEntity.getBrand());
            StockDetailDTO stockDetailDTO = new StockDetailDTO(productMapper.mapProductEntityToProductDto(productEntity,
                    categoryDTO,
                    brandDTO), stock);
            return stockDetailDTO;
        } else {
            throw new NotFoundException("No se encontró producto para mostrar stock");
        }
    }


    public ResponseEntity<String> createStock(StockNewDTO stockNewDTO) {
        StockEntity stockEntity = stockMapper.mapStockDTOToStockEntity(stockNewDTO,
                findProduct(stockNewDTO.getProduct_id()),
                findSubsidiary(stockNewDTO.getSubsidiary_id()),
                findUser(stockNewDTO.getUser_id()));
        stockRepository.save(stockEntity);

        return new ResponseEntity<>("Created", HttpStatus.OK);
    }

    public ResponseEntity<String> deleteStock(int id) {
        Optional<StockEntity> optionalStockEntity = stockRepository.findById(id);

        if (optionalStockEntity.isPresent()) {
            StockEntity stockEntity = optionalStockEntity.get();
            stockEntity.setDeleted(new Date());
            stockRepository.save(stockEntity);

            return new ResponseEntity<>("Deleted", HttpStatus.OK);
        } else {
            throw new NotFoundException("No se encontró stock");
        }

    }

    public ResponseEntity<String> editStock(StockEditDTO stockEditDTO) {
        Optional<StockEntity> optionalStockEntity = stockRepository.findById(stockEditDTO.getId());

        if (optionalStockEntity.isPresent()) {
            StockEntity stockEntity = stockMapper.mapStockEditDTOToStockEntity(stockEditDTO,
                    findProduct(stockEditDTO.getProduct_id()),
                    findSubsidiary(stockEditDTO.getSubsidiary_id()),
                    findUser(stockEditDTO.getUser_id()),
                    optionalStockEntity.get().getDeleted(),
                    optionalStockEntity.get().getCreated());
            stockEntity.setId(stockEditDTO.getId());

            stockRepository.save(stockEntity);

            return new ResponseEntity<>("Modified", HttpStatus.OK);
        } else {
            throw new NotFoundException("No existe stock");
        }
    }

    public ArrayList<Object> getStocksDTOParams(StockEntity stockEntity) {
        ProductDTO productDTO = productMapper.mapProductEntityToProductDto(stockEntity.getProduct(),
                categoryMapper.mapCategoryEntityToCategoryDto(stockEntity.getProduct().getCategory()),
                brandMapper.mapBrandEntityToBrandDTO(stockEntity.getProduct().getBrand()));
        SubsidiaryDTO subsidiaryDTO = subsidiaryMapper.mapSubsidiaryEntityToSubsidiaryDTO(stockEntity.getSubsidiary());
        UserMinDTO userMinDto = userMapper.mapUserEntityToUserMinDTO(stockEntity.getUser());

        ArrayList<Object> objectArrayList = new ArrayList<>();
        objectArrayList.add(productDTO);
        objectArrayList.add(subsidiaryDTO);
        objectArrayList.add(userMinDto);
        return objectArrayList;
    }

    public ProductEntity findProduct(int product_id) {
        Optional<ProductEntity> optionalProductEntity = productRepository.findById(product_id);

        if (optionalProductEntity.isEmpty()) {
            throw new NotFoundException("No existe Product");
        }
        return optionalProductEntity.get();
    }

    public SubsidiaryEntity findSubsidiary(int subsidiary_id) {
        Optional<SubsidiaryEntity> optionalBrandEntity = subsidiaryRepository.findById(subsidiary_id);

        if (optionalBrandEntity.isEmpty()) {
            throw new NotFoundException("No existe Subsidiary");
        }
        return optionalBrandEntity.get();
    }

    public UserEntity findUser(int user_dni) {
        Optional<UserEntity> optionalUserEntity = userRepository.findByDni(user_dni);

        if (optionalUserEntity.isEmpty()) {
            throw new NotFoundException("No existe User");
        }
        return optionalUserEntity.get();
    }
}
