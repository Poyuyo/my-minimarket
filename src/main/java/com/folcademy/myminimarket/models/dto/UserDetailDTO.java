package com.folcademy.myminimarket.models.dto;

import com.folcademy.myminimarket.models.entities.SubsidiaryEntity;

import java.util.Date;

public class UserDetailDTO {


    private int dni;
    private String name;
    private SubsidiaryEntity subsidiary;
    private Date deleted;
    private Date created;
    private Date modified;

    public UserDetailDTO() {
    }

    public UserDetailDTO(int dni, String name, SubsidiaryEntity subsidiary, Date deleted, Date created, Date modified) {
        this.dni = dni;
        this.name = name;
        this.subsidiary = subsidiary;
        this.deleted = deleted;
        this.created = created;
        this.modified = modified;
    }

    public int getDni() {
        return dni;
    }

    public String getName() {
        return name;
    }

    public SubsidiaryEntity getSubsidiary() {
        return subsidiary;
    }

    public Date getDeleted() {
        return deleted;
    }

    public Date getCreated() {
        return created;
    }

    public Date getModified() {
        return modified;
    }
}
