package com.folcademy.myminimarket.models.dto;

public class CategoryNewDTO {
    private String name;

    public CategoryNewDTO() {
    }

    public CategoryNewDTO(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
