package com.folcademy.myminimarket.models.entities;

import com.folcademy.myminimarket.models.enums.MeasureUnit;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;

@Entity(name = "products")
public class ProductEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;
    private String name;
    private String observations;
    @Enumerated(EnumType.STRING)
    private MeasureUnit measure_unit;
    private BigDecimal unit_amount;
    @ManyToOne()
    @JoinColumn(name = "category_id")
    private CategoryEntity category;
    @ManyToOne()
    @JoinColumn(name = "brand_id")
    private BrandEntity brand;
    @Temporal(TemporalType.DATE)
    private Date expiration_data;
    @Temporal(TemporalType.DATE)
    private Date deleted;
    @Temporal(TemporalType.DATE)
    private Date created;
    @Temporal(TemporalType.DATE)
    private Date modified;

    public ProductEntity() {
    }

    public ProductEntity(String name, String observations, MeasureUnit measure_unit, BigDecimal unit_amount, CategoryEntity category, BrandEntity brand, Date expiration_data, Date deleted, Date created, Date modified) {
        this.name = name;
        this.observations = observations;
        this.measure_unit = measure_unit;
        this.unit_amount = unit_amount;
        this.category = category;
        this.brand = brand;
        this.expiration_data = expiration_data;
        this.deleted = deleted;
        this.created = created;
        this.modified = modified;
    }

    public Integer getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getObservations() {
        return observations;
    }

    public MeasureUnit getMeasure_unit() {
        return measure_unit;
    }

    public BigDecimal getUnit_amount() {
        return unit_amount;
    }

    public CategoryEntity getCategory() {
        return category;
    }

    public BrandEntity getBrand() {
        return brand;
    }

    public Date getExpiration_data() {
        return expiration_data;
    }

    public Date getDeleted() {
        return deleted;
    }

    public Date getCreated() {
        return created;
    }

    public Date getModified() {
        return modified;
    }

    public void setCategory(CategoryEntity category) {
        this.category = category;
    }

    public void setBrand(BrandEntity brand) {
        this.brand = brand;
    }

    public void setDeleted(Date deleted) {
        this.deleted = deleted;
    }

    public void setId(int id) {
        this.id = id;
    }
}
