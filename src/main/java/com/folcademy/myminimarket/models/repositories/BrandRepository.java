package com.folcademy.myminimarket.models.repositories;

import com.folcademy.myminimarket.models.entities.BrandEntity;
import com.folcademy.myminimarket.models.entities.CategoryEntity;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface BrandRepository extends CrudRepository<BrandEntity, String> {
    Optional<BrandEntity> findById(int id);
}
