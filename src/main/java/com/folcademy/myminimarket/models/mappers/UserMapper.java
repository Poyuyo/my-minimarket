package com.folcademy.myminimarket.models.mappers;

import com.folcademy.myminimarket.models.dto.*;
import com.folcademy.myminimarket.models.entities.SubsidiaryEntity;
import com.folcademy.myminimarket.models.entities.UserEntity;
import org.springframework.stereotype.Component;

import java.util.Date;

@Component
public class UserMapper {
    public UserDTO mapUserEntityToUserDTO(UserEntity userEntity, SubsidiaryDTO subsidiaryDTO) {
        return new UserDTO(userEntity.getDni(),
                userEntity.getName(),
               subsidiaryDTO);
    }

    public UserMinDTO mapUserEntityToUserMinDTO(UserEntity userEntity){
        return new UserMinDTO(userEntity.getDni(), userEntity.getName());
    }

    public UserDetailDTO mapUserEntityToUserDetailDto(UserEntity userEntity){
        return new UserDetailDTO(userEntity.getDni(),
                userEntity.getName(),
                userEntity.getSubsidiary(),
                userEntity.getDeleted(),
                userEntity.getCreated(),
                userEntity.getModified());
    }

    public UserEntity mapUserNewDtoToUserEntity(UserNewDTO userNewDTO, SubsidiaryEntity subsidiaryEntity){
        return new UserEntity(userNewDTO.getDni(),
                userNewDTO.getName(),
                userNewDTO.getUsername(),
                userNewDTO.getPassword(),
                subsidiaryEntity,
                null,
                new Date(),
                new Date());
    }

    public UserEntity mapUserNewDtoToUserEntity(UserNewDTO userNewDTO, SubsidiaryEntity subsidiaryEntity, Date deleted, Date created){
        return new UserEntity(userNewDTO.getDni(),
                userNewDTO.getName(),
                userNewDTO.getUsername(),
                userNewDTO.getPassword(),
                subsidiaryEntity,
                deleted,
                created,
                new Date());
    }
}
